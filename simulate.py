import argparse
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
from random import random, randrange
import sys
from typing import Iterable
from typing import Tuple, List
from typing import Callable

Terrain = Iterable[Iterable[int]]

from agent_utils import moore_neighberhood, von_neumann_neighberhood, climb_steepest, climb_steepest_or_random
from Agent import Agent

neighborhoods = {
    'MOORE': moore_neighberhood,
    'VON_NEUMANN': von_neumann_neighberhood
}

techniques = {
    'STEEPEST': climb_steepest,
    'STEEPEST_OR_RANDOM': climb_steepest_or_random
}

init_distributions = {
    'RANDOM': lambda n, rows, cols: [(randrange(rows), randrange(cols)) for i in range(n)],
    'QUARTER': lambda n, rows, cols: [(randrange(rows//4), randrange(cols//4)) for i in range(n)],
    'FIRST_COL': lambda n, rows, cols: [(randrange(rows), 0) for i in range(n)]
}

ROWS = 100
COLS = 100


def main(terrain_path, n_iter, n_agents, reach, init_dist, neighborhood, technique, animate):
    with open(terrain_path, 'rb') as f:
        terrain = np.load(f)
    if terrain.shape != (ROWS, COLS):
        sys.exit('Wrong terrain shape (should be 100x100)')
    maximum = np.max(terrain) 
    initial_positions = init_dist(n_agents, ROWS, COLS)
    agents = [Agent(pos[0], pos[1], reach, neighborhood, technique, ROWS, COLS, terrain) for pos in initial_positions]
    agents_position_history = []
    print('Running the simulation...')
    for _ in range(n_iter):
        current_agent_positions = []
        for agent in agents:
            if animate is not None:
                current_agent_positions.append((agent.x, agent.y))
            agent.climb()
        if animate is not None:
            agents_position_history.append(current_agent_positions)
    if animate is not None:
        print('Rendering the simulation visualization...')
        norm_terrain = np.zeros((terrain.shape[0], terrain.shape[1], 3))
        for i in range(ROWS):
            for j in range(COLS):
                norm_terrain[i][j] = (0, terrain[i][j]/maximum, 0)
        fig, ax = plt.subplots(figsize=(10,10))
        plt.axis('off')
        ims = []
        for positions in agents_position_history:
            frame = np.copy(norm_terrain)
            for position in positions:
                x, y = position
                frame[x][y] = (1, 0, 0) # RED
                im = ax.imshow(frame, animated=True)
                ims.append([im])
        ani = animation.ArtistAnimation(fig, ims, interval=200)
        print('Saving the visualization...')
        ani.save(animate, writer=animation.FFMpegWriter(fps=60))
    agents_positions = [(agent.x, agent.y) for agent in agents]
    heights = [terrain[x][y] for x, y in agents_positions]
    found_max = max(heights)
    print(
        '''
        Terrain max:\t{terrain_max}
        Found max:\t{found_max}
        '''.format(
            terrain_max=maximum,
            found_max=found_max
        )
    )


if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Agent based simulation of mountain climbing.')
    parser.add_argument('--terrain', action='store', dest='TERRAIN', default=None, type=str, help='Path to the file containing the terrain for the simulation.')
    parser.add_argument('--iter', action='store', dest='N_ITER', default=1000, type=int, help='Number of simulation iterations.')
    parser.add_argument('--agents', action='store', dest='N_AGENTS', default=1, type=int, help='Number of agents.')
    parser.add_argument('--reach', action='store', dest='REACH', default=1, type=int, help='Agent reach.')
    parser.add_argument('--init', action='store', dest='AGENTS_INIT', default='RANDOM', type=str, help='Initial distribution of agents (RANDOM, QUARTER or FIRST_COL).')
    parser.add_argument('--neighborhood', action='store', dest='NEIGHBORHOOD', default='MOORE', type=str, help='Neighborhood type (MOORE or VON_NEUMANN)')
    parser.add_argument('--technique', action='store', dest='TECHNIQUE', default='STEEPEST', type=str, help='Agent climbing technique (STEEPEST or STEEPEST_OR_RANDOM)')
    parser.add_argument('--threshold', action='store', dest='THRESHOLD', default=0.5, type=float, help='Threshold for STEEPEST_OR_RANDOM climbing technique.')
    parser.add_argument('--animate', action='store', dest='ANIMATE', default=None, type=str, help='Path and filename to save the animation (if left as None, animation will not be saved)')

    args = parser.parse_args()

    if args.TERRAIN is None:
        sys.exit('Terrain cannot be empty.')
    else:
        if args.NEIGHBORHOOD in neighborhoods:
            neighborhood = neighborhoods.get(args.NEIGHBORHOOD)
        else:
            sys.exit('Unknown neighborhood.')
        if args.TECHNIQUE in techniques:
            if args.TECHNIQUE == 'STEEPEST_OR_RANDOM':
                technique = techniques.get(args.TECHNIQUE)(args.THRESHOLD)
            else:
                technique = techniques.get(args.TECHNIQUE)
        else:
            sys.exit('Unknown climbing technique.')
        if args.AGENTS_INIT in init_distributions:
            init_dist = init_distributions.get(args.AGENTS_INIT)
        else:
            sys.exit('Unknown initial distribution of agents.')
        main(
            args.TERRAIN,
            args.N_ITER,
            args.N_AGENTS,
            args.REACH,
            init_dist,
            neighborhood,
            technique,
            args.ANIMATE
        )

