from typing import Iterable
from typing import Tuple, List
from typing import Callable
from random import random, randrange

Terrain = Iterable[Iterable[int]]


def moore_neighberhood(x: int, y: int, rows: int, cols: int, terrain: Terrain) -> List[Tuple[int, int, int]]:
    """Return a tuple containing neighbors coordinates and height.
    """
    in_bounds = lambda x, y: 0 <= x < rows and 0 <= y < cols
    neighbors = []
    for i in range(-1, 2):
        for j in range(-1, 2):
            row = x+i
            col = y+j
            if in_bounds(row, col) and (i != 0 or j != 0):
                neighbors.append(tuple((row, col, terrain[row][col])))
    return neighbors


def von_neumann_neighberhood(x: int, y: int, rows: int, cols: int, terrain: Terrain) -> List[Tuple[int, int, int]]:
    in_bounds = lambda x, y: 0 <= x < rows and 0 <= y < cols
    neighbors = []
    for i in range(-1, 2):
        for j in range(-1, 2):
            row = x+i
            col = y+j
            if in_bounds(row, col) and (i == 0 or j == 0) and i != j:
                neighbors.append(tuple((row, col, terrain[row][col])))
    return neighbors


def climb_steepest(
    x: int,
    y: int,
    reach: int,
    neighbors: List[Tuple[int, int, int]],
    current_height: int
) -> Tuple[int, int]:
    """Go to the tallest point in the neighborhood within reach.
    """
    
    possible_directions = [(x, y, height - current_height) for (x, y, height) in neighbors
                           if 0 <= height - current_height <= reach]
    if possible_directions:
        steepest = max(possible_directions, key=lambda x_y_diff: x_y_diff[2])
        return steepest[:2]
    else:
        return tuple((x, y))


def climb_steepest_or_random(threshold: float) -> Callable:
    def climb(
        x: int,
        y: int,
        reach: int,
        neighbors: List[Tuple[int, int, int]],
        current_height: int
    ) -> Tuple[int, int]:

        possible_directions = [(x, y, height - current_height) for (x, y, height) in neighbors
                               if height - current_height <= reach]
        if possible_directions:
            if random() < threshold:
                index = randrange(len(possible_directions))
                return possible_directions[index][:2]
            else:
                steepest = max(possible_directions, key=lambda x_y_diff: x_y_diff[2])
                if steepest[2] > 0:
                    return steepest[:2]
                else:
                    return tuple((x, y))
        else:
            return tuple((x, y))
        
    return climb
