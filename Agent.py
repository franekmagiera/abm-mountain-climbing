from typing import Iterable
from typing import Tuple, List
from typing import Callable

Terrain = Iterable[Iterable[int]]

class Agent:
    """Mountain climber.
    """
    def __init__(
        self,
        x: int,
        y: int,
        reach: int,
        neighborhood_type: Callable,
        climbing_technique: Callable,
        rows: int,
        cols: int,
        terrain: Terrain
    ):
        self.x = x
        self.y = y
        self.reach = reach
        self.neighborhood_type = neighborhood_type
        self.climbing_technique = climbing_technique
        self.rows = rows
        self.cols = cols
        self.terrain = terrain
    
    
    def get_neighbors(self):
        return self.neighborhood_type(self.x, self.y, self.rows, self.cols, self.terrain)
    
    
    def set_position(self, new_x, new_y):
        self.x = new_x
        self.y = new_y
    
    
    def climb(self):
        current_height = self.terrain[self.x][self.y]
        new_position = self.climbing_technique(self.x, self.y, self.reach, self.get_neighbors(), current_height)
        self.set_position(*new_position)

